angular.module('app').factory('postService', function ($http) {
    var postService = {};

    var baseURL = "https://jsonplaceholder.typicode.com/";

    postService.getPosts = function () {
        return $http.get(baseURL + 'posts');
    }

    postService.getPostComments = function(id) {
        return $http.get(baseURL + 'comments?postId=' + id);
    }

    return postService;
});