angular.module('app').factory('userService', function ($http) {
    
    var userService = {};
    var baseURL = "https://jsonplaceholder.typicode.com/";
    var localURL = "http://localhost/ad4pixel-api/"

    userService.getUserInfo = function (id) {
        return $http.get('https://jsonplaceholder.typicode.com/users/' + id);
    }

    userService.getUserPhotos = function (id) {
        return $http.get(localURL + "photos.php?id=" + id);
    }

    return userService;
});