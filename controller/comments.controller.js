angular.module('app').controller('commentsController', function ($scope, postService) {

    $scope.comments = [];

    var postId = $scope.post.id;
    
    postService.getPostComments(postId).then(function successCallback(response) {
        $scope.comments = response.data;
    }, function errorCallback(response) {
        console.log(response);
    });

});
