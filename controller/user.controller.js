angular.module('app').controller('userController', function ($scope, userService) {

    $scope.user = {};
    $scope.photos = {};
    var userId = 1;

    userService.getUserInfo(userId).then(function successCallback(response) {
        var data = response.data;
        $scope.user = data;
        $scope.address = data.address;
    }, function errorCallback(response) {
        console.log(response);
    });

    userService.getUserPhotos(userId).then(function successCallback(response) {
        for (i = 0; i < 10; i++) {
            $scope.photos[i] = res.data[i];
        }
    }, function errorCallback(response) {
        console.log(response);
    });
});