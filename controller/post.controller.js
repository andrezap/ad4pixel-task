angular.module('app').controller('postController', function ($scope, postService) {

    $scope.posts = {};

    postService.getPosts().then(function successCallback(response) {
        $scope.posts = response.data;
    }, function errorCallback(response) {
        console.log(response);
    });


});