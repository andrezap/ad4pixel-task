angular.module('app', ['ui.router'])
    .config(['$stateProvider', '$urlRouterProvider',
        function ($stateProvider, $urlRouterProvider) {
            $urlRouterProvider.otherwise('/');
            $stateProvider
                .state('home', {
                    url: '/',
                    views: {
                        'header': {
                            templateUrl: 'views/header.html',
                            controller: ''
                        },                                            
                        'profile': {
                            templateUrl: 'views/profile.html',
                            controller: 'userController'
                        },
                        'feed': {
                            templateUrl: 'views/feed.html',
                            controller: 'postController'
                        },
                        'infos': {
                            templateUrl: 'views/info.html',
                            controller: ''
                        }
                    }
                });
        }
    ]);