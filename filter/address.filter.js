angular.module('app').filter('formatAddress', function () {
  return function (item) {
      if(item)
        return item.street + ',' + item.city;
  };
});