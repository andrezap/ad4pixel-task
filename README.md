Project built with 

* Angular 1.6
* Bootstrap 3
* Gulp

### RUNNING

Please, install the dependencies before:

`npm install`

and run the project 

`gulp` or `npm run gulp`
